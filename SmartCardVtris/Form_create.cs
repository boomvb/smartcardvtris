﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using ThaiNationalIDCard;
using Zen.Barcode;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;

namespace SmartCardVtris
{
    public partial class Form_create : Form
    {
        private SqlConnection connection = new SqlConnection();
        private SqlConnection connection2 = new SqlConnection();
        private string GenIDVolunteer;
        private string firstname;
        private string title;
        private string lastname;
        private string idcard;
        private Bitmap imgProfile;
        public Form_create()
        {
            InitializeComponent();
            connection.ConnectionString = @"Data Source=10.188.128.31;Initial Catalog=trc-vtris-25620823;User ID=root;Password=root";
            connection2.ConnectionString = @"Data Source=localhost;Initial Catalog=db_volunteer;User ID=sa;Password=boom1234;Pooling=False";
        }
        private void Form_create_Load(object sender, EventArgs e)
        {
            ThaiIDCard idcard = new ThaiIDCard();
            filedTitle();
            filedProvince();
            filedAmphur();
            filedBlood();
            filedMajor();
            filedGender();
            filedNationality();
            filedClub();
            filedOcWork();
            filedOcNotWork();

            filedpreferWorkingProvince();
            //AutoComplete//
            listbox_title.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            listbox_title.AutoCompleteSource = AutoCompleteSource.ListItems;

            listbox_major.AutoCompleteMode = AutoCompleteMode.Suggest;
            listbox_major.AutoCompleteSource = AutoCompleteSource.ListItems;

            listbox_province.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            listbox_province.AutoCompleteSource = AutoCompleteSource.ListItems;

            listbox_amphur.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            listbox_amphur.AutoCompleteSource = AutoCompleteSource.ListItems;

            listbox_club.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            listbox_club.AutoCompleteSource = AutoCompleteSource.ListItems;

            list_occupationwork.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            list_occupationwork.AutoCompleteSource = AutoCompleteSource.ListItems;

            list_occupationNotwork.Visible = false;
            list_occupationwork.Visible = false;

            groupClub.Visible = false;

            majorclub.Visible = false;
            majorNotclub.Visible = false;



            LoadGridView();

        }
        private void LoadGridView()
        {
            connection2.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection2;
            string query = "SELECT v.identity_card_number as เลขบัตรประชาชน,t.name as คำนำหน้าชื่อ,v.first_name as ชื่อ,v.last_name as นามสกุล,v.mobile_number as เบอร์โทร,v.email as อีเมล์ FROM db_volunteer.volunteer v INNER JOIN db_volunteer.title t on t.id = v.title_id";
            cmd.CommandText = query;
            SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            dAdapter.Fill(ds);
            data_volunteer.DataSource = ds.Tables[0].DefaultView;
            connection2.Close();
        }
        private void btn_readCard_Click(object sender, EventArgs e)
        {
            try
            {
                Refresh();
                ThaiIDCard idcard = new ThaiIDCard();
                idcard.eventPhotoProgress += new handlePhotoProgress(photoProgress);
                Personal personal = idcard.readAllPhoto();
                if (personal != null)
                {
                    txt_identity_card_number.Text = personal.Citizenid;
                    txt_birthday.Text = personal.Birthday.ToString("dd/MM/yyyy");

                    if (personal.Sex == "1")
                    {
                        listbox_gender.Text = "ชาย";
                    }
                    else if (personal.Sex == "2")
                    {
                        listbox_gender.Text = "หญิง";
                    }
                    listbox_title.Text = personal.Th_Prefix;
                    txt_firstname.Text = personal.Th_Firstname;
                    txt_lastname.Text = personal.Th_Lastname;
                    picture_personal.Image = personal.PhotoBitmap;

                    //Address//
                    txt_housenumber.Text = personal.addrHouseNo;
                    txt_moo.Text = personal.addrVillageNo;
                    txt_soi.Text = personal.addrRoad;
                    txt_road.Text = personal.addrLane;
                    string personalAddTambol = personal.addrTambol;
                    string subTambols = personalAddTambol.Substring(4);
                    txt_tambon.Text = subTambols;

                    string personelAddAmphurs = personal.addrAmphur;
                    string subAmphurs = "";
                    if (personelAddAmphurs.Length > 5)
                    {
                        subAmphurs = personelAddAmphurs.Substring(5);
                    }else if(personelAddAmphurs.Length > 3)
                    {
                        subAmphurs = personelAddAmphurs.Substring(3);
                    }else if(personelAddAmphurs.Length > 2)
                    {
                        subAmphurs = personelAddAmphurs.Substring(2);
                    }
                    listbox_amphur.Text = subAmphurs;

                    string personelAddProvinces = personal.addrProvince;
                    string subProvinces = "";
                    if(personelAddProvinces.Length > 7)
                    {
                        subProvinces = personelAddProvinces.Substring(7);
                    }else if(personelAddProvinces.Length > 2)
                    {
                        subProvinces = personelAddProvinces.Substring(2);
                    }else
                    {
                        subProvinces = personelAddProvinces;
                    }
                    
                    listbox_province.Text = subProvinces;

                    MessageBox.Show("ดึงข้อมูลบัตรเรียบร้อยแล้ว");

                    listbox_major.Text = "";
                    listbox_nationality.Text = "";
                    txt_phone.Text = "";
                }
                else if (idcard.ErrorCode() > 0)
                {
                    MessageBox.Show(idcard.Error());
                }
                else
                {
                    MessageBox.Show("Catch all");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void ClearData()
        {
            preferWorkingProvince.Text = "";
            majorclub.Visible = false;
            txt_identity_card_number.Text = "";
            listbox_title.Text = "";
            txt_firstname.Text = "";
            txt_lastname.Text = "";
            listbox_gender.Text = "";
            txt_birthday.Text = "";
            listbox_nationality.Text = "";
            listbox_blood.Text = "";
            txt_phone.Text = "";
            txt_housenumber.Text = "";
            txt_moo.Text = "";
            txt_placename.Text = "";
            txt_village.Text = "";
            txt_soi.Text = "";
            txt_road.Text = "";
            txt_tambon.Text = "";
            listbox_amphur.Text = "";
            listbox_province.Text = "";
            txt_postcode.Text = "";
            listbox_major.Text = "";
            if (picture_personal.Image != null)
            {
                picture_personal.Image.Dispose();
                picture_personal.Image = null;
            }
        }
        private void LogLine(string address)
        {
            throw new NotImplementedException();
        }
        private void photoProgress(int value, int maximum)
        {

            if (txtBoxLog.InvokeRequired)
            {
                if (StatusProgressBar.Maximum != maximum)
                    StatusProgressBar.BeginInvoke(new MethodInvoker(delegate { StatusProgressBar.Maximum = maximum; }));

                // fix progress bar sync.
                if (StatusProgressBar.Maximum > value)
                    StatusProgressBar.BeginInvoke(new MethodInvoker(delegate { StatusProgressBar.Value = value + 1; }));

                StatusProgressBar.BeginInvoke(new MethodInvoker(delegate { StatusProgressBar.Value = value; }));
            }
            else
            {
                if (StatusProgressBar.Maximum != maximum)
                    StatusProgressBar.Maximum = maximum;

                // fix progress bar sync.
                if (StatusProgressBar.Maximum > value)
                    StatusProgressBar.Value = value + 1;

                StatusProgressBar.Value = value;
                if (StatusProgressBar.Value == maximum)
                {
                    StatusProgressBar.Value = 0;
                }
            }
        }
        //คำนวณอายุ
        public string CalAge(String dateDOB)
        {
            int now = int.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
            int dob = int.Parse(dateDOB);
            string dif = (now - dob).ToString();
            string age = "0";
            if (dif.Length > 4)
            {
                age = dif.Substring(0, dif.Length - 4);
            }
            return age;
        }
        private void filedTitle()
        {
            try
            {
                if (connection.State == ConnectionState.Closed) { connection.Open(); }
                string query = "select id,name from title";
                SqlDataAdapter dAdapter = new SqlDataAdapter(query, connection);
                DataTable source = new DataTable();
                dAdapter.Fill(source);
                DataRow dr;
                dr = source.NewRow();
                dr.ItemArray = new object[] { 0, "" };
                source.Rows.InsertAt(dr, 0);
                listbox_title.DataSource = source;
                listbox_title.ValueMember = "id";
                listbox_title.DisplayMember = "name";
                listbox_titles.DataSource = source;
                listbox_titles.ValueMember = "id";
                listbox_titles.DisplayMember = "name";
                if (connection.State != ConnectionState.Closed) { connection.Close(); }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().ToString(), "Error In Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private void filedpreferWorkingProvince()
        {
            try
            {
                if (connection.State == ConnectionState.Closed) { connection.Open(); }
                string query = "select id,thai_name from province";
                SqlDataAdapter dAdapter = new SqlDataAdapter(query, connection);
                DataTable source = new DataTable();
                dAdapter.Fill(source);
                DataRow dr;
                dr = source.NewRow();
                dr.ItemArray = new object[] { 0, "" };
                source.Rows.InsertAt(dr, 0);
                preferWorkingProvince.DataSource = source;
                preferWorkingProvince.ValueMember = "id";
                preferWorkingProvince.DisplayMember = "thai_name";
                if (connection.State != ConnectionState.Closed) { connection.Close(); }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().ToString(), "Error In Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void filedProvince()
        {
            try
            {
                if (connection.State == ConnectionState.Closed) { connection.Open(); }
                string query = "select id,thai_name from province";
                SqlDataAdapter dAdapter = new SqlDataAdapter(query, connection);
                DataTable source = new DataTable();
                dAdapter.Fill(source);
                DataRow dr;
                dr = source.NewRow();
                dr.ItemArray = new object[] { 0, "" };
                source.Rows.InsertAt(dr, 0);
                listbox_province.DataSource = source;
                listbox_province.ValueMember = "id";
                listbox_province.DisplayMember = "thai_name";
                if (connection.State != ConnectionState.Closed) { connection.Close(); }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().ToString(), "Error In Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void filedAmphur()
        {
            try
            {
                if (connection.State == ConnectionState.Closed) { connection.Open(); }
                string query = "select id,name from amphur";
                SqlDataAdapter dAdapter = new SqlDataAdapter(query, connection);
                DataTable source = new DataTable();
                dAdapter.Fill(source);
                DataRow dr;
                dr = source.NewRow();
                dr.ItemArray = new object[] { 0, "" };
                source.Rows.InsertAt(dr, 0);
                listbox_amphur.DataSource = source;
                listbox_amphur.ValueMember = "id";
                listbox_amphur.DisplayMember = "name";
                if (connection.State != ConnectionState.Closed) { connection.Close(); }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().ToString(), "Error In Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void filedBlood()
        {
            try
            {
                listbox_blood.DisplayMember = "Text";
                listbox_blood.ValueMember = "Value";
                var itemBlood = new[]
                {
                    new  { Text = "", Value = "" },
                    new  { Text = "A", Value = "A" },
                    new  { Text = "B", Value = "B" },
                    new  { Text = "AB", Value = "AB" },
                    new  { Text = "O", Value = "O" },
                    new { Text = "A Rh-", Value = "A Rh-" },
                    new { Text = "B Rh-", Value = "B Rh-" },
                    new { Text = "AB Rh-", Value = "AB Rh-" },
                    new { Text = "O Rh-", Value = "O Rh-"},
                };
                listbox_blood.DataSource = itemBlood;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().ToString(), "Error In Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void filedMajor()
        {
            try
            {
                if (connection2.State == ConnectionState.Closed) { connection2.Open(); }
                string query = "select id,name_th from db_volunteer.major";
                SqlDataAdapter dAdapter = new SqlDataAdapter(query, connection2);
                DataTable source = new DataTable();
                dAdapter.Fill(source);
                DataRow dr;
                dr = source.NewRow();
                dr.ItemArray = new object[] { 0, "" };
                source.Rows.InsertAt(dr, 0);
                listbox_major.DataSource = source;
                listbox_major.ValueMember = "id";
                listbox_major.DisplayMember = "name_th";
                if (connection2.State != ConnectionState.Closed) { connection2.Close(); }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().ToString(), "Error In Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void filedGender()
        {
            try
            {
                var itemSex = new[]
                {
                    new { Text = "", Value = "" },
                    new { Text = "ชาย", Value = "MALE" },
                    new { Text = "หญิง", Value = "FEMALE" }
                };
                listbox_gender.DataSource = itemSex;
                listbox_gender.DisplayMember = "Text";
                listbox_gender.ValueMember = "Value";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().ToString(), "Error In Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void filedNationality()
        {
            try
            {
                if (connection2.State == ConnectionState.Closed) { connection2.Open(); }
                string query = "select id,nation_name_th from db_volunteer.citizenship";
                SqlDataAdapter dAdapter = new SqlDataAdapter(query, connection2);
                DataTable source = new DataTable();
                dAdapter.Fill(source);
                DataRow dr;
                dr = source.NewRow();
                dr.ItemArray = new object[] { 0, "" };
                source.Rows.InsertAt(dr, 0);
                listbox_nationality.DataSource = source;
                listbox_nationality.ValueMember = "id";
                listbox_nationality.DisplayMember = "nation_name_th";
                if (connection2.State != ConnectionState.Closed) { connection2.Close(); }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().ToString(), "Error In Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void filedClub()
        {
            try
            {
                if (connection.State == ConnectionState.Closed) { connection.Open(); }
                string query = "select id,name from club";
                SqlDataAdapter dAdapter = new SqlDataAdapter(query, connection);
                DataTable source = new DataTable();
                dAdapter.Fill(source);
                DataRow dr;
                dr = source.NewRow();
                dr.ItemArray = new object[] { 0, "" };
                source.Rows.InsertAt(dr, 0);
                listbox_club.DataSource = source;
                listbox_club.ValueMember = "id";
                listbox_club.DisplayMember = "name";
                if (connection.State != ConnectionState.Closed) { connection.Close(); }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().ToString(), "Error In Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void filedOcWork()
        {
            try
            {
                var itemOcWork = new[]
                {
                    new { Text = "", Value = "" },
                    new { Text = "รับราชการ", Value = "SERVE" },
                    new { Text = "พนักงานรัฐวิสาหกิจ/บริษัทเอกชน", Value = "EMPLOYEE" },
                    new { Text = "รับจ้างใช้แรงงาน", Value = "LABOR_CONTACT" },
                    new { Text = "เจ้าของกิจการ", Value = "TITLEHOLDER" },
                    new { Text = "อาชีพอิสระ", Value = "FREELANCE" },
                    new { Text = "เกษตรกร", Value = "ARICULTURIST"}
                };
                list_occupationwork.DataSource = itemOcWork;
                list_occupationwork.DisplayMember = "Text";
                list_occupationwork.ValueMember = "Value";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().ToString(), "Error In Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void filedOcNotWork()
        {
            try
            {
                var itemOcNotWork = new[]
                {
                    new { Text = "", Value = "" },
                    new { Text = "พระภิกษุ/นักบวช", Value = "MONK_AND_PRIEST" },
                    new { Text = "พ่อบ้าน/แม่บ้าน", Value = "BUTLER_OR_MAID" },
                    new { Text = "อาสาสมัคร", Value = "VOLUNTEER" },
                    new { Text = "เกษียณ", Value = "RETIRE" },
                    new { Text = "ไม่ได้ประกอบอาชีพ", Value = "SUPERANNUATE"},
                    new { Text = "นักเรียน/นักศึกษา", Value = "STUDENT"}
                };
                list_occupationNotwork.DataSource = itemOcNotWork;
                list_occupationNotwork.DisplayMember = "Text";
                list_occupationNotwork.ValueMember = "Value";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().ToString(), "Error In Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string convertDate(string str_date)
        {
                int Day = Int32.Parse(str_date.Split("/".ToCharArray())[0]);
                int Month = Int32.Parse(str_date.Split("/".ToCharArray())[1]);
                int Year = Int32.Parse(str_date.Split("/".ToCharArray())[2]);
                int St_year = Year - 543;
                string string_dateConvert = "" + St_year + "-" + Month + "-" + Day;

                return string_dateConvert;
        }
        private void btn_readBackCard_Click(object sender, EventArgs e)
        {
            PrintDialog pd = new PrintDialog();
            PrintDocument pdoc = new PrintDocument();
            PrinterSettings ps = new PrinterSettings();
            Font font = new Font("EucrosiaUPC", 15);
            PaperSize psize = new PaperSize("A4", 827, 1169);

            pd.Document = pdoc;
            pd.Document.DefaultPageSettings.PaperSize = psize;
            pdoc.PrintPage += new PrintPageEventHandler(printbackcard_PrintPage);

            DialogResult result = pd.ShowDialog();
            if (result == DialogResult.OK)
            {
                PrintPreviewDialog pp = new PrintPreviewDialog();
                pp.Document = pdoc;
                pd.Document.DefaultPageSettings.PaperSize = psize;
                pdoc.PrintPage += new PrintPageEventHandler(printbackcard_PrintPage);
                pp.ShowDialog();
                //pdoc.Print();
            }
        }
        private void btn_saveData_Click(object sender, EventArgs e)
        {
            try
            {
                string WorkingProvince = preferWorkingProvince.Text;
                idcard = txt_identity_card_number.Text;
                idcard = idcard.Replace("-", "");
                title = listbox_title.Text;
                firstname = txt_firstname.Text;
                lastname = txt_lastname.Text;
                string gender = listbox_gender.Text;
                string birthday = txt_birthday.Text;
                string nationality = listbox_nationality.Text;
                string blood = listbox_blood.Text;
                string major = listbox_major.Text;
                string phone = txt_phone.Text;
                string email = txt_email.Text;
                string club = listbox_club.Text;
                //Address//
                string house_number = txt_housenumber.Text;
                string moo = txt_moo.Text;
                string placename = txt_placename.Text;
                string village = txt_village.Text;
                string soi = txt_soi.Text;
                string road = txt_road.Text;
                string tambon = txt_tambon.Text;
                string amphur = listbox_amphur.Text;
                string province = listbox_province.Text;
                string postcode = txt_postcode.Text;
                bool isEduChecked = education_underdegree.Checked;
                if (isEduChecked)
                {
                   
                }
                //Validate//
                if(WorkingProvince == "")
                {
                    MessageBox.Show("กรุณาเลือกจังหวัดที่ต้องการอาสา", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                if (idcard == " -    -     -  -")
                {
                    MessageBox.Show("กรุณากรอกเลขบัตรประชาชน", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txt_identity_card_number.Focus();
                    return;
                }
                else if (idcard.Length < 13)
                {
                    MessageBox.Show("กรุณาระบุเลขบัตรประชาชนให้ครบ 13 หลัก", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txt_identity_card_number.Focus();
                }
                else if(title == "")
                {
                    MessageBox.Show("กรุณาเลือกคำนำหน้าชื่อ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    listbox_title.Focus();
                    return;
                }else if(firstname == "")
                {
                    MessageBox.Show("กรุณากรอกชื่อ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txt_firstname.Focus();
                    return;
                }else if(lastname == "")
                {
                    MessageBox.Show("กรุณากรอกนามสกุล", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txt_lastname.Focus();
                    return;
                }else if(gender == "")
                {
                    MessageBox.Show("กรุณาเลือกเพศ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    listbox_gender.Focus();
                    return;
                }else
                {
                    if (connection.State == ConnectionState.Closed){ connection.Open();}
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "SELECT Count(*) FROM volunteer WHERE identity_card_number='" + idcard + "' ";
                    int rowCount = (int)command.ExecuteScalar();
                    if (rowCount >= 1)
                    {
                        MessageBox.Show("เลขบัตรประชาชนนี้ มีข้อมูลในระบบแล้ว กรุณาลองใหม่อีกครั้ง", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else
                    {
                        //GenIDANDSaveImage//
                        command.CommandText = "SELECT id FROM volunteer";
                        SqlDataAdapter cAapter = new SqlDataAdapter(command);
                        DataSet dt = new DataSet();
                        cAapter.Fill(dt);
                        int RowCount = dt.Tables[0].Rows.Count;
                        GenIDVolunteer = "00000" + (RowCount + 1);
                        string str_filename = null;
                        if (picture_personal.Image != null)
                        {
                            imgProfile = new Bitmap(picture_personal.Image);
                            str_filename = "ImgProfile" + GenIDVolunteer + ".png";
                            imgProfile.Save(@"D:\\ImgProfile\" + str_filename);
                        }
                        else
                        {
                            str_filename = "NULL";
                        }
                        //RemoveFormatidcard//
                        string commaidcard = idcard;
                        commaidcard = commaidcard.Replace("-", "");
                        //ConvertThaiGender//
                        if (listbox_gender.Text == "MALE")
                        {
                            gender = "ชาย";
                        }
                        else if (listbox_gender.Text == "FEMALE")
                        {
                            gender = "หญิง";
                        }
                        //ConvertDatetime//
                        string iDate = null;
                        string str_BirthDate = null;
                        DateTime oDate = DateTime.MinValue;
                        if (birthday != "  /  /")
                        {
                            iDate = birthday;
                            oDate = Convert.ToDateTime(iDate);
                            str_BirthDate = oDate.Year + "-" + oDate.Month + "-" + oDate.Day;
                        }
                        else
                        {
                            str_BirthDate = null;
                        }

                        //InsertVolunteer//
                        string query = "INSERT INTO volunteer (version,identity_card_number,title_id,first_name,last_name,code,computer_skill_email_internet,computer_skill_graphic,computer_skill_office,computer_skill_programming,education_id,job_convenient_fri_afternoon,job_convenient_fri_evening,job_convenient_fri_morning,job_convenient_mon_afternoon,job_convenient_mon_evening,job_convenient_mon_morning,job_convenient_sat_afternoon,job_convenient_sat_evening,job_convenient_sat_morning,job_convenient_sun_afternoon,job_convenient_sun_evening,job_convenient_sun_morning,job_convenient_thu_afternoon,job_convenient_thu_evening,job_convenient_thu_morning,job_convenient_tue_afternoon,job_convenient_tue_evening,job_convenient_tue_morning,job_convenient_wed_afternoon,job_convenient_wed_evening,job_convenient_wed_morning,job_interest_communications,job_interest_disaster_services,job_interest_fund_raising,job_interest_general_services,job_interest_health_care,job_interest_organ_donation_services,job_interest_quality_of_life_development,job_interest_secretary_and_document_services,language_skill_bahasa,language_skill_chinese,language_skill_english,language_skill_filipino,language_skill_french,language_skill_german,language_skill_japanese,language_skill_khmer,language_skill_korean,language_skill_lao,language_skill_malay,language_skill_myanmar,language_skill_spanish,language_skill_vietnam,job_interest_care_children,job_interest_care_patients,job_interest_care_seniors,job_interest_cooking,job_interest_labour,gender,birth_date,nationality_id,line_id,blood_type,major,mobile_number,email,club_id,profile_image_path,date_created,office,office_reg,registered_group_id,prefer_working_province_id) ";
                        query += "VALUES (@version,@identity_card_number,@title_id,@first_name,@last_name,@code,@computer_skill_email_internet,@computer_skill_graphic,@computer_skill_office,@computer_skill_programming,@education_id,@job_convenient_fri_afternoon,@job_convenient_fri_evening,@job_convenient_fri_morning,@job_convenient_mon_afternoon,@job_convenient_mon_evening,@job_convenient_mon_morning,@job_convenient_sat_afternoon,@job_convenient_sat_evening,@job_convenient_sat_morning,@job_convenient_sun_afternoon,@job_convenient_sun_evening,@job_convenient_sun_morning,@job_convenient_thu_afternoon,@job_convenient_thu_evening,@job_convenient_thu_morning,@job_convenient_tue_afternoon,@job_convenient_tue_evening,@job_convenient_tue_morning,@job_convenient_wed_afternoon,@job_convenient_wed_evening,@job_convenient_wed_morning,@job_interest_communications,@job_interest_disaster_services,@job_interest_fund_raising,@job_interest_general_services,@job_interest_health_care,@job_interest_organ_donation_services,@job_interest_quality_of_life_development,@job_interest_secretary_and_document_services,@language_skill_bahasa,@language_skill_chinese,@language_skill_english,@language_skill_filipino,@language_skill_french,@language_skill_german,@language_skill_japanese,@language_skill_khmer,@language_skill_korean,@language_skill_lao,@language_skill_malay,@language_skill_myanmar,@language_skill_spanish,@language_skill_vietnam,@job_interest_care_children,@job_interest_care_patients,@job_interest_care_seniors,@job_interest_cooking,@job_interest_labour,@gender,@birth_date,@nationality_id,@line_id,@blood_type,@major,@mobile_number,@email,@club_id,@profile_image_path,@date_created,@office,@office_reg,@registered_group_id,@prefer_working_province_id)";
                        command.Connection = connection;
                        command.CommandText = query;
                        command.Parameters.AddWithValue("@version", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@identity_card_number", SqlDbType.VarChar).Value = commaidcard;
                        command.Parameters.AddWithValue("@title_id", SqlDbType.Int).Value = listbox_title.SelectedValue;
                        command.Parameters.AddWithValue("@first_name", SqlDbType.VarChar).Value = firstname;
                        command.Parameters.AddWithValue("@last_name", SqlDbType.VarChar).Value = lastname;
                        command.Parameters.AddWithValue("@code", SqlDbType.DateTime).Value = "NULL";
                        command.Parameters.AddWithValue("@computer_skill_email_internet", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@computer_skill_graphic", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@computer_skill_office", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@computer_skill_programming", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@education_id", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_convenient_fri_afternoon", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_convenient_fri_evening", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_convenient_fri_morning", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_convenient_mon_afternoon", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_convenient_mon_evening", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_convenient_mon_morning", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_convenient_sat_afternoon", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_convenient_sat_evening", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_convenient_sat_morning", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_convenient_sun_afternoon", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_convenient_sun_evening", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_convenient_sun_morning", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_convenient_thu_afternoon", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_convenient_thu_evening", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_convenient_thu_morning", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_convenient_tue_afternoon", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_convenient_tue_evening", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_convenient_tue_morning", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_convenient_wed_afternoon", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_convenient_wed_evening", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_convenient_wed_morning", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_interest_communications", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_interest_disaster_services", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_interest_fund_raising", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_interest_general_services", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_interest_health_care", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_interest_organ_donation_services", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_interest_quality_of_life_development", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@job_interest_secretary_and_document_services", SqlDbType.Int).Value = 0;
                        command.Parameters.AddWithValue("@language_skill_bahasa", SqlDbType.TinyInt).Value = 0;
                        command.Parameters.AddWithValue("@language_skill_chinese", SqlDbType.TinyInt).Value = 0;
                        command.Parameters.AddWithValue("@language_skill_english", SqlDbType.TinyInt).Value = 0;
                        command.Parameters.AddWithValue("@language_skill_filipino", SqlDbType.TinyInt).Value = 0;
                        command.Parameters.AddWithValue("@language_skill_french", SqlDbType.TinyInt).Value = 0;
                        command.Parameters.AddWithValue("@language_skill_german", SqlDbType.TinyInt).Value = 0;
                        command.Parameters.AddWithValue("@language_skill_japanese", SqlDbType.TinyInt).Value = 0;
                        command.Parameters.AddWithValue("@language_skill_khmer", SqlDbType.TinyInt).Value = 0;
                        command.Parameters.AddWithValue("@language_skill_korean", SqlDbType.TinyInt).Value = 0;
                        command.Parameters.AddWithValue("@language_skill_lao", SqlDbType.TinyInt).Value = 0;
                        command.Parameters.AddWithValue("@language_skill_malay", SqlDbType.TinyInt).Value = 0;
                        command.Parameters.AddWithValue("@language_skill_myanmar", SqlDbType.TinyInt).Value = 0;
                        command.Parameters.AddWithValue("@language_skill_spanish", SqlDbType.TinyInt).Value = 0;
                        command.Parameters.AddWithValue("@language_skill_vietnam", SqlDbType.TinyInt).Value = 0;
                        command.Parameters.AddWithValue("@job_interest_care_children", SqlDbType.TinyInt).Value = 0;
                        command.Parameters.AddWithValue("@job_interest_care_patients", SqlDbType.TinyInt).Value = 0;
                        command.Parameters.AddWithValue("@job_interest_care_seniors", SqlDbType.TinyInt).Value = 0;
                        command.Parameters.AddWithValue("@job_interest_cooking", SqlDbType.TinyInt).Value = 0;
                        command.Parameters.AddWithValue("@job_interest_labour", SqlDbType.TinyInt).Value = 0;
                        command.Parameters.AddWithValue("@gender", SqlDbType.VarChar).Value = gender;
                        command.Parameters.AddWithValue("@birth_date", SqlDbType.DateTime).Value = str_BirthDate;
                        command.Parameters.AddWithValue("@nationality_id", SqlDbType.VarChar).Value = listbox_nationality.SelectedValue;
                        command.Parameters.AddWithValue("@line_id", SqlDbType.VarChar).Value = lineId.Text;
                        command.Parameters.AddWithValue("@blood_type", SqlDbType.VarChar).Value = blood;
                        command.Parameters.AddWithValue("@major", SqlDbType.Int).Value = listbox_major.SelectedValue;
                        command.Parameters.AddWithValue("@mobile_number", SqlDbType.VarChar).Value = phone;
                        command.Parameters.AddWithValue("@email", SqlDbType.Text).Value = email;
                        command.Parameters.AddWithValue("@club_id", SqlDbType.Int).Value = listbox_club.SelectedValue;
                        command.Parameters.AddWithValue("@profile_image_path", SqlDbType.Text).Value = str_filename;
                        command.Parameters.AddWithValue("@date_created", SqlDbType.DateTime).Value = DateTime.Now;
                        command.Parameters.AddWithValue("@office", SqlDbType.VarChar).Value = "YV";
                        command.Parameters.AddWithValue("@office_reg", SqlDbType.VarChar).Value = "YV";
                        command.Parameters.AddWithValue("@registered_group_id", SqlDbType.Int).Value = 1;
                        command.Parameters.AddWithValue("@prefer_working_province_id", SqlDbType.Int).Value = preferWorkingProvince.SelectedValue;
                        command.ExecuteNonQuery();

                        int result = command.ExecuteNonQuery();
                        if (result > 0)
                        {
                            int getIdVolunteer = (int)command.ExecuteScalar();
                            string queryVolunteerAddress = "INSERT INTO volunteer_address ([id],[version],[amphur_id],[country_id],[house_number],[place_name],[post_code],[province_id],[road],[soi],[tumbone],[village],[village_number],[REF]) VALUES (@id,@version,@amphur_id,@country_id,@house_number,@place_name,@post_code,@province_id,@road,@soi,@tumbone,@village,@village_number,@REF)";
                            command.Connection = connection;
                            command.CommandText = queryVolunteerAddress;
                            //Address//
                            command.Parameters.AddWithValue("@id", getIdVolunteer);
                            command.Parameters.AddWithValue("@version", "NULL");
                            command.Parameters.AddWithValue("@amphur_id", amphur);
                            command.Parameters.AddWithValue("@country_id", 1);
                            command.Parameters.AddWithValue("@house_number", house_number);
                            command.Parameters.AddWithValue("@place_name", placename);
                            command.Parameters.AddWithValue("@post_code", postcode);
                            command.Parameters.AddWithValue("@province_id", listbox_province);
                            command.Parameters.AddWithValue("@road", road);
                            command.Parameters.AddWithValue("@soi", soi);
                            command.Parameters.AddWithValue("@tumbone", tambon);
                            command.Parameters.AddWithValue("@village", moo);
                            command.Parameters.AddWithValue("@village_number", moo);
                            command.Parameters.AddWithValue("@REF", "NULL");
                            command.ExecuteNonQuery();

                            if (connection.State != ConnectionState.Closed) { connection.Close(); }

                            MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว", "", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            ClearData();
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void printfrontcard_PrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;
            SolidBrush Brush = new SolidBrush(Color.Black);
            StringFormat sf = new StringFormat();

            //แนวนอนบน//
            Pen myPenFront = new Pen(Color.Black);
            myPenFront.Width = 2;
            //graphics.DrawLine(myPenFront, 0, 220, 335, 220);

            //แนวนอนล่าง
            myPenFront = new Pen(Color.Black);
            myPenFront.Width = 2;
            graphics.DrawLine(myPenFront, 0, 335, 220, 335);

            // แนวตั้งขวา//
            myPenFront = new Pen(Color.Black);
            myPenFront.Width = 2;
            graphics.DrawLine(myPenFront, 220, 0, 220, 335);

            //แนวตั้งซ้าย//
            myPenFront = new Pen(Color.Black);
            myPenFront.Width = 1;
            graphics.DrawLine(myPenFront, 0, 75, 0, 335);

            string nameevent = "งานกาชาดรวมพลจิตอาสา";
            string nameevent2 = "17 มี.ค. 62";
            string nameevent3 = "ณ สยามสแควร์วัน";
            string textrow = this.GenIDVolunteer;
            string textname = this.title + this.firstname;
            string textlastname = this.lastname;
            //string textrow = "0000002000";
            //string textname = "ไซดอาหมัดอับดุลเลาะ";
            //string textlastname = "ปาลกะวงศ์ ณ อยุธยา";

            //logo//
            Bitmap bmplogofront = Properties.Resources.logo_TRCsVolunteer_front;
            bmplogofront.SetResolution(300, 300);
            Image logofront = bmplogofront;
            Rectangle rect = new Rectangle(5, 5, 65, 65);
            graphics.DrawImage(logofront, rect);

            //nameevent//
            graphics.DrawString(nameevent, new Font("EucrosiaUPC", 12, FontStyle.Bold), Brushes.Black, new Point(75, 10));

            //nameevent2//
            graphics.DrawString(nameevent2, new Font("EucrosiaUPC", 10, FontStyle.Bold), Brushes.Black, new Point(75, 27));

            //nameevent3//
            graphics.DrawString(nameevent3, new Font("EucrosiaUPC", 10, FontStyle.Bold), Brushes.Black, new Point(75, 40));

            //textjit//
            Bitmap bmpjitsa = Properties.Resources.tab_jit;
            bmpjitsa.SetResolution(300, 300);
            Image jitsa = bmpjitsa;
            Rectangle rectjitsa = new Rectangle(0, 80, 67, 255);
            graphics.DrawImage(jitsa, rectjitsa);

            //imageprofile//
            if (picture_personal.Image != null)
            {
                Bitmap bmpprofile = this.imgProfile;
                bmpprofile.SetResolution(300, 300);
                Image profile = bmpprofile;
                Rectangle rectprofile = new Rectangle(110, 75, 80, 95);
                graphics.DrawImage(profile, rectprofile);
            }
            else
            {
                Bitmap bmpprofile = Properties.Resources.logo_vb;
                bmpprofile.SetResolution(300, 300);
                Image profile = bmpprofile;
                Rectangle rectprofile = new Rectangle(103, 75, 85, 95);
                graphics.DrawImage(profile, rectprofile);
            }

            //textname//
            if (textname.Length < 17)
            {
                sf.LineAlignment = StringAlignment.Center;
                sf.Alignment = StringAlignment.Center;
                graphics.DrawString(textname, new Font("EucrosiaUPC", 13, FontStyle.Bold), Brushes.Black, new Point(145, 190), sf);
            }
            else
            {
                sf.LineAlignment = StringAlignment.Center;
                sf.Alignment = StringAlignment.Center;
                graphics.DrawString(textname, new Font("EucrosiaUPC", 13, FontStyle.Bold), Brushes.Black, new Point(145, 190), sf);
            }
            //textlastname//
            if (textlastname.Length < 21)
            {
                sf.LineAlignment = StringAlignment.Center;
                sf.Alignment = StringAlignment.Center;
                graphics.DrawString(textlastname, new Font("EucrosiaUPC", 13, FontStyle.Bold), Brushes.Black, new Point(145, 215), sf);
            }
            else
            {
                sf.LineAlignment = StringAlignment.Center;
                sf.Alignment = StringAlignment.Center;
                graphics.DrawString(textlastname, new Font("EucrosiaUPC", 13, FontStyle.Bold), Brushes.Black, new Point(140, 210), sf);
            }
            //textrow//
            graphics.DrawString(textrow, new Font("EucrosiaUPC", 14, FontStyle.Bold), Brushes.Black, new Point(145, 260), sf);


            //barcode//
            Code128BarcodeDraw barcode = BarcodeDrawFactory.Code128WithChecksum;
            Image imgbarcode = barcode.Draw(this.idcard, 60);
            Rectangle rectimgbarcode = new Rectangle(85, 270, 120, 25);
            graphics.DrawImage(imgbarcode, rectimgbarcode);

            //logovtris//
            Bitmap bmpvtris = Properties.Resources.logo_vtris;
            bmpvtris.SetResolution(300, 300);
            Image vtris = bmpvtris;
            Rectangle rectvtris = new Rectangle(117, 305, 60, 18);
            graphics.DrawImage(vtris, rectvtris);
        }

        private void printbackcard_PrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;

            graphics.SmoothingMode = SmoothingMode.AntiAlias;
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

            SolidBrush Brush = new SolidBrush(Color.Black);
            string textArsa = "สำนักงานอาสากาชาด สภากาชาดไทย";
            string textAddArsa = "ถนนพระราม 4 ปทุมวัน กทม.";
            string text6 = "ขอขอบคุณที่สนใจร่วมเป็นจิตอาสาสภากาชาดไทย";
            string text7 = "ติดตามการอบรม/กิจกรรม ในช่องทางคิวอาร์โค้ดนี้";

            //แนวนอนบน//
            Pen myPenBack = new Pen(Color.Black);
            myPenBack.Width = 2;
            //graphics.DrawLine(myPenFront, 0, 220, 335, 220);

            //แนวนอนล่าง//
            myPenBack = new Pen(Color.Black);
            myPenBack.Width = 2;
            graphics.DrawLine(myPenBack, 0, 335, 220, 335);

            // แนวตั้งขวา//
            myPenBack = new Pen(Color.Black);
            myPenBack.Width = 2;
            graphics.DrawLine(myPenBack, 220, 0, 220, 335);

            //แนวตั้งซ้าย//
            myPenBack = new Pen(Color.Red);
            myPenBack.Width = 100;
            graphics.DrawLine(myPenBack, 0, 60, 0, 335);

            //text6//
            graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            graphics.TextContrast = 0;
            graphics.DrawString(text6, new Font("EucrosiaUPC", 10, FontStyle.Bold), Brushes.Black, new Point(6, 15));
            //text7//
            graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            graphics.TextContrast = 0;
            graphics.DrawString(text7, new Font("EucrosiaUPC", 10, FontStyle.Bold), Brushes.Black, new Point(3, 30));

            //Qrcode//
            CodeQrBarcodeDraw qrcode = BarcodeDrawFactory.CodeQr;
            Image imgqrcode = qrcode.Draw("http://volunteerspirit.redcross.or.th/", 60);
            Rectangle rectQrcode = new Rectangle(95, 80, 90, 90);
            graphics.DrawImage(imgqrcode, rectQrcode);

            //LogoTrcsจิตอาสา//
            Bitmap Logotrcs = Properties.Resources.logo_TRCsVolunteer_back;
            Logotrcs.SetResolution(1200F, 1200F);
            Image Trcs = Logotrcs;
            Rectangle rectTrcs = new Rectangle(80, 180, 120, 85);
            graphics.DrawImage(Trcs, rectTrcs);

            //LogoVB//
            Bitmap bmpLogoVb = Properties.Resources.logo_vb;
            bmpLogoVb.SetResolution(1200F, 1200F);
            Image LogoVb = bmpLogoVb;
            Rectangle rectLogoVb = new Rectangle(55, 300, 22, 22);
            graphics.DrawImage(LogoVb, rectLogoVb);

            //TextArsa//
            graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            graphics.TextContrast = 0;
            graphics.DrawString(textArsa, new Font("EucrosiaUPC", 8, FontStyle.Bold), Brushes.Black, new Point(85, 300));

            //textAddArsa//
            graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            graphics.TextContrast = 0;
            graphics.DrawString(textAddArsa, new Font("EucrosiaUPC", 8, FontStyle.Bold), Brushes.Black, new Point(85, 310));
        }

        private void btn_searchData_Click(object sender, EventArgs e)
        {
            try
            {
                string search = txt_search.Text;
                if (search == "")
                {
                    MessageBox.Show("กรุณาระบุคำค้นหา (เลขบัตรประชาชน)", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txt_search.Focus();
                }
                else
                {
                    if (connection.State == ConnectionState.Closed){connection.Open();}
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "SELECT * from volunteers WHERE identity_card_number LIKE '%" + txt_search.Text.Trim() + "%' ORDER BY identity_card_number ASC";
                    SqlDataAdapter query = new SqlDataAdapter(command);
                    DataSet ds = new DataSet();
                    query.Fill(ds);
                    int CountRows = ds.Tables[0].Rows.Count;
                    if(CountRows >= 1)
                    {
                        string idRow = ds.Tables[0].Rows[0]["ID"].ToString();
                        txt_idno.Text = idRow;
                        string identity_card_number = ds.Tables[0].Rows[0]["identity_card_number"].ToString();
                        txt_idcard.Text = identity_card_number;
                        string title = ds.Tables[0].Rows[0]["title_id"].ToString();
                        listbox_titles.Text = title;
                        string first_name = ds.Tables[0].Rows[0]["first_name"].ToString();
                        txt_firstnames.Text = first_name;
                        string last_name = ds.Tables[0].Rows[0]["last_name"].ToString();
                        txt_lastnames.Text = last_name;
                        string mobile_number = ds.Tables[0].Rows[0]["phone"].ToString();
                        txt_phones.Text = mobile_number;
                        string imgProfile = ds.Tables[0].Rows[0]["profile_image_path"].ToString();
                        if (imgProfile != "")
                        {
                            picture_personals.Image = Image.FromFile(@"D:\\ImgProfile\" + imgProfile);
                        }
                        else
                        {
                            picture_personals.Image = Properties.Resources.no_imgjit;
                        }
                    }
                    else
                    {
                        MessageBox.Show("ไม่พบข้อมูลที่ค้นหา กรุณาลองใหม่อีกครั้ง!", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        ClearData();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_PrintCardSerach_Click(object sender, EventArgs e)
        {
            try
            {
                string idno = txt_idno.Text;
                string identity_card_number = txt_idcard.Text;
                string titles = listbox_titles.Text;
                string first_name = txt_firstnames.Text;
                string last_name = txt_lastnames.Text;
                if (picture_personals.Image != null)
                {
                    imgProfile = new Bitmap(picture_personals.Image);

                }
                else
                {
                    //imgProfile = null;
                }
                if (idno != "" && identity_card_number != "" && titles != "" && first_name != "")
                {
                    DialogResult dialogResult = MessageBox.Show("ต้องการพิมพ์บัตร ??", "แจ้งเตือน", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {

                        PrintDialog pd = new PrintDialog();
                        PrintDocument pdoc = new PrintDocument();
                        PrinterSettings ps = new PrinterSettings();
                        Font font = new Font("EucrosiaUPC", 15);
                        PaperSize psize = new PaperSize("A4", 827, 1169);

                        pd.Document = pdoc;
                        pd.Document.DefaultPageSettings.PaperSize = psize;
                        pdoc.PrintPage += new PrintPageEventHandler(printfrontcard_PrintPage);
                        pdoc.Print();

                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        if (Application.OpenForms.Count == 0) Application.Exit();
                    }
                    ClearData();
                }else
                {
                    MessageBox.Show("ยังไม่ได้กรอกข้อมูล กรุณาลองใหม่อีกครั้ง!", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        private void IsNumeric(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && (e.KeyChar != 8))
            {
                e.Handled = true;
            }
        }

        private void listbox_province_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (listbox_province.Text != "")
                {
                    if (connection.State == ConnectionState.Closed) { connection.Open(); }
                    string query = "select id,name from amphur WHERE province_id = " + listbox_province.SelectedValue + "";
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, connection);
                    DataTable source = new DataTable();
                    dAdapter.Fill(source);
                    DataRow dr;
                    dr = source.NewRow();
                    dr.ItemArray = new object[] { 0, "" };
                    source.Rows.InsertAt(dr, 0);
                    listbox_amphur.DataSource = source;
                    listbox_amphur.ValueMember = "id";
                    listbox_amphur.DisplayMember = "name";
                    if (connection.State != ConnectionState.Closed) { connection.Close(); }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().ToString(), "Error In Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tabControl1_Click(object sender, EventArgs e)
        {
            LoadGridView();
        }

        private void occupationGroupwork_CheckedChanged(object sender, EventArgs e)
        {
            if (occupationGroupwork.Checked == true)
            {
                list_occupationwork.Refresh();
                list_occupationNotwork.Refresh();

                list_occupationwork.Visible = true;
                list_occupationNotwork.Visible = false;
            }
            else
            {
                list_occupationwork.Refresh();
                list_occupationNotwork.Refresh();

                list_occupationwork.Visible = false;
                list_occupationNotwork.Visible = true;
            }
        }

        private void occupationGroupnotwork_CheckedChanged(object sender, EventArgs e)
        {
            if (occupationGroupnotwork.Checked == true)
            {
                list_occupationwork.Refresh();
                list_occupationNotwork.Refresh();

                list_occupationwork.Visible = false;
                list_occupationNotwork.Visible = true;
            }
            else
            {
                list_occupationwork.Refresh();
                list_occupationNotwork.Refresh();

                list_occupationwork.Visible = true;
                list_occupationNotwork.Visible = false;

            }
        }

        private void listbox_major_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(listbox_major.Text == "ครู/อาจารย์")
            {
                majorclub.Visible = true;
                majorNotclub.Visible = true;
            }
            else
            {
                majorclub.Visible = false;
                majorNotclub.Visible = false;
            }
        }

        private void majorclub_CheckedChanged(object sender, EventArgs e)
        {
            if (majorclub.Checked == true)
            {
                groupClub.Visible = true;
            }
            else
            {
                groupClub.Visible = false;
            }
        }

        private void list_occupationNotwork_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (list_occupationNotwork.Text == "นักเรียน/นักศึกษา")
            {
                majorclub.Visible = true;
                majorNotclub.Visible = true;
            }
            else
            {
                majorclub.Visible = false;
                majorNotclub.Visible = false;
            }
        }

        private void majorNotclub_CheckedChanged(object sender, EventArgs e)
        {
            if (majorNotclub.Checked == true)
            {
                groupClub.Visible = false;
            }
            else
            {
                groupClub.Visible = true;
            }
        }

        private void preferWorkingProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if(preferWorkingProvince.Text != "")
                {
                    if (connection.State == ConnectionState.Closed) { connection.Open(); }
                    string query = "select id,name from club WHERE province_id = " + preferWorkingProvince.SelectedValue + "";
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, connection);
                    DataTable source = new DataTable();
                    dAdapter.Fill(source);
                    DataRow dr;
                    dr = source.NewRow();
                    dr.ItemArray = new object[] { 0, "" };
                    source.Rows.InsertAt(dr, 0);
                    listbox_club.DataSource = source;
                    listbox_club.ValueMember = "id";
                    listbox_club.DisplayMember = "name";
                    if (connection.State != ConnectionState.Closed) { connection.Close(); }
                }
            }
            catch
            {

            }
        }
    }
}
