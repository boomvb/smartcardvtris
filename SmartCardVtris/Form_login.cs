﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartCardVtris
{
    public partial class Form_login : Form
    {
        private OleDbConnection connection = new OleDbConnection();
        public Form_login()
        {
            InitializeComponent();
            connection.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\Application\vBApp\SmartCardVtris\SmartCardVtris\db_volunteer.accdb;Persist Security Info=True";
        }

        private void Form_login_Load(object sender, EventArgs e)
        {
            try
            {
                connection.Open();
                Console.WriteLine("Connection Success");
                connection.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Err"+ ex);
            }
        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            try
            {
                string username = txt_Username.Text;
                string password = txt_Password.Text;
                if(username == "")
                {
                    MessageBox.Show("กรุณากรอกชื่อผู้ใช้งาน", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txt_Username.Focus();
                    return;
                }else if(password == "")
                {
                    MessageBox.Show("กรุณากรอกรหัสผ่าน", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txt_Password.Focus();
                    return;
                }
                else
                {
                    connection.Open();
                    OleDbCommand command = new OleDbCommand();
                    command.Connection = connection;
                    command.CommandText = "select * from users where username='" + username + "' and password='" + password + "'";

                    OleDbDataReader readerUser = command.ExecuteReader();
                    int count = 0;
                    while (readerUser.Read())
                    {
                        count = count + 1;
                    }
                    if (count == 1)
                    {
                        MessageBox.Show("ชื่อผู้ใช้งานหรือรหัสผ่าน ถูกต้อง");
                        this.Hide();
                        Form_create formCreate = new Form_create();
                        formCreate.Show();
                    }
                    else
                    {
                        MessageBox.Show("ชื่อผู้ใช้งานหรือรหัสผ่าน ไม่ถูกต้องกรุณาลองใหม่อีกครั้ง", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        clearData();
                    }
                    connection.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Err" + ex);
            }
        }
        private void clearData()
        {
            txt_Username.Text = "";
            txt_Password.Text = "";
        }

        private void btn_closeApp_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
