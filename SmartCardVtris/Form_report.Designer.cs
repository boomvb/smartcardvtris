﻿namespace SmartCardVtris
{
    partial class Form_report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_report));
            this.label1 = new System.Windows.Forms.Label();
            this.label_report = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label_time = new System.Windows.Forms.Label();
            this.label_date1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("PSL Baannarak", 99.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.Location = new System.Drawing.Point(-1, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(811, 161);
            this.label1.TabIndex = 0;
            this.label1.Text = "รายงานจำนวนผู้สมัคร";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_report
            // 
            this.label_report.AutoSize = true;
            this.label_report.Font = new System.Drawing.Font("PSL Baannarak", 90F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_report.ForeColor = System.Drawing.Color.DarkRed;
            this.label_report.Location = new System.Drawing.Point(269, 265);
            this.label_report.Name = "label_report";
            this.label_report.Size = new System.Drawing.Size(108, 145);
            this.label_report.TabIndex = 1;
            this.label_report.Text = "0";
            this.label_report.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("PSL Baannarak", 90F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.Location = new System.Drawing.Point(426, 264);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(156, 145);
            this.label2.TabIndex = 2;
            this.label2.Text = "คน";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("PSL Baannarak", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.Location = new System.Drawing.Point(151, 165);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 45);
            this.label3.TabIndex = 3;
            this.label3.Text = "ณ วันที่";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_time
            // 
            this.label_time.AutoSize = true;
            this.label_time.Font = new System.Drawing.Font("PSL Baannarak", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_time.ForeColor = System.Drawing.Color.MediumBlue;
            this.label_time.Location = new System.Drawing.Point(503, 165);
            this.label_time.Name = "label_time";
            this.label_time.Size = new System.Drawing.Size(116, 45);
            this.label_time.TabIndex = 4;
            this.label_time.Text = "00:00:00";
            this.label_time.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_date1
            // 
            this.label_date1.AutoSize = true;
            this.label_date1.Font = new System.Drawing.Font("PSL Baannarak", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_date1.ForeColor = System.Drawing.Color.MediumBlue;
            this.label_date1.Location = new System.Drawing.Point(247, 165);
            this.label_date1.Name = "label_date1";
            this.label_date1.Size = new System.Drawing.Size(29, 45);
            this.label_date1.TabIndex = 5;
            this.label_date1.Text = "-";
            this.label_date1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("PSL Baannarak", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.Location = new System.Drawing.Point(432, 165);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 45);
            this.label4.TabIndex = 6;
            this.label4.Text = "เวลา";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("PSL Baannarak", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.Location = new System.Drawing.Point(627, 165);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 45);
            this.label5.TabIndex = 7;
            this.label5.Text = "น.";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form_report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(808, 467);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label_date1);
            this.Controls.Add(this.label_time);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label_report);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_report";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "รายงานจำนวนผู้สมัคร";
            this.Load += new System.EventHandler(this.Form_report_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_report;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label_time;
        private System.Windows.Forms.Label label_date1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}