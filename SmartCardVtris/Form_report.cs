﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Globalization;

namespace SmartCardVtris
{
    public partial class Form_report : Form
    {
        Timer t = new Timer();
        public Form_report()
        {
            InitializeComponent();
        }

        private void Form_report_Load(object sender, EventArgs e)
        {
            //LoadTime//
            t.Interval = 1000;
            t.Tick += new EventHandler(this.t_Tick);
            //start timer when form loads
            t.Start();  //this will use t_Tick() method
        }
        
        private void t_Tick(Object sender, EventArgs e)
        {
            int hh = DateTime.Now.Hour;
            int mm = DateTime.Now.Minute;
            int ss = DateTime.Now.Second;

            string time = "";
            if (hh < 10)
            {
                time += "0" + hh;
            }
            else
            {
                time += hh;
            }
            time += ":";

            if (mm < 10)
            {
                time += "0" + mm;
            }
            else
            {
                time += mm;
            }
            time += ":";

            if (ss < 10)
            {
                time += "0" + ss;
            }
            else
            {
                time += ss;
            }

            label_time.Text = time;

            DateTime date1 = new DateTime(2019, 3, 17);
            //Console.WriteLine(date1.ToString("d MMMM yyyy", CultureInfo.CreateSpecificCulture("th-TH")));
            label_date1.Text = date1.ToString("d MMMM yyyy", CultureInfo.CreateSpecificCulture("th-TH"));

            string ConStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='D:\\volunteer.xlsx';Extended Properties=\"Excel 12.0;ReadOnly=False;HDR=Yes;IMEX=0;\"";
            OleDbConnection conn = new OleDbConnection(ConStr);
            System.Data.OleDb.OleDbDataAdapter cmd2;
            cmd2 = new System.Data.OleDb.OleDbDataAdapter("select ID,identity_card_number,first_name,last_name,gender,date_created from [volunteer$]", conn);
            // copy the Excel value to the DataSet   
            DataSet ds = new System.Data.DataSet();
            cmd2.Fill(ds);
            int rows = ds.Tables[0].Rows.Count;

            label_report.Text = rows.ToString();

        }
    }
}
