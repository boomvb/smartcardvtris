﻿using System.Windows.Forms;

namespace SmartCardVtris
{
    partial class Form_create
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_create));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lineId = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.preferWorkingProvince = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txt_identity_card_number = new System.Windows.Forms.MaskedTextBox();
            this.txt_birthday = new System.Windows.Forms.MaskedTextBox();
            this.txt_phone = new System.Windows.Forms.TextBox();
            this.btn_readBackCard = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txt_placename = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txt_housenumber = new System.Windows.Forms.TextBox();
            this.listbox_province = new System.Windows.Forms.ComboBox();
            this.txt_road = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txt_soi = new System.Windows.Forms.TextBox();
            this.txt_postcode = new System.Windows.Forms.TextBox();
            this.txt_village = new System.Windows.Forms.TextBox();
            this.txt_tambon = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.listbox_amphur = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txt_moo = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.btn_saveData = new System.Windows.Forms.Button();
            this.btn_readCard = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.listbox_blood = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.listbox_nationality = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.listbox_major = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.listbox_gender = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.picture_personal = new System.Windows.Forms.PictureBox();
            this.txt_lastname = new System.Windows.Forms.TextBox();
            this.txt_firstname = new System.Windows.Forms.TextBox();
            this.listbox_title = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.StatusProgressBar = new System.Windows.Forms.ProgressBar();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txt_idcard = new System.Windows.Forms.MaskedTextBox();
            this.btn_PrintCardSerach = new System.Windows.Forms.Button();
            this.txt_idno = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.txt_phones = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label46 = new System.Windows.Forms.Label();
            this.picture_personals = new System.Windows.Forms.PictureBox();
            this.txt_lastnames = new System.Windows.Forms.TextBox();
            this.txt_firstnames = new System.Windows.Forms.TextBox();
            this.listbox_titles = new System.Windows.Forms.ComboBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btn_searchData = new System.Windows.Forms.Button();
            this.txt_search = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btn_exportExcel = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.data_volunteer = new System.Windows.Forms.DataGridView();
            this.txtBoxLog = new System.Windows.Forms.Label();
            this.printbackcard = new System.Drawing.Printing.PrintDocument();
            this.printfrontcard = new System.Drawing.Printing.PrintDocument();
            this.listbox_club = new System.Windows.Forms.ComboBox();
            this.groupClub = new System.Windows.Forms.GroupBox();
            this.list_occupationwork = new System.Windows.Forms.ComboBox();
            this.list_occupationNotwork = new System.Windows.Forms.ComboBox();
            this.majorclub = new System.Windows.Forms.RadioButton();
            this.majorNotclub = new System.Windows.Forms.RadioButton();
            this.occupationGroupwork = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.occupationGroupnotwork = new System.Windows.Forms.RadioButton();
            this.groupeducation = new System.Windows.Forms.GroupBox();
            this.education_underdegree = new System.Windows.Forms.RadioButton();
            this.education_bachelor = new System.Windows.Forms.RadioButton();
            this.education_masterdegree = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picture_personal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picture_personals)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.data_volunteer)).BeginInit();
            this.groupClub.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupeducation.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.pictureBox1.BackColor = System.Drawing.Color.DarkRed;
            this.pictureBox1.Location = new System.Drawing.Point(0, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(926, 69);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupeducation);
            this.groupBox1.Controls.Add(this.groupBox6);
            this.groupBox1.Controls.Add(this.groupClub);
            this.groupBox1.Controls.Add(this.majorNotclub);
            this.groupBox1.Controls.Add(this.majorclub);
            this.groupBox1.Controls.Add(this.list_occupationNotwork);
            this.groupBox1.Controls.Add(this.list_occupationwork);
            this.groupBox1.Controls.Add(this.lineId);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.preferWorkingProvince);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txt_email);
            this.groupBox1.Controls.Add(this.label35);
            this.groupBox1.Controls.Add(this.txt_identity_card_number);
            this.groupBox1.Controls.Add(this.txt_birthday);
            this.groupBox1.Controls.Add(this.txt_phone);
            this.groupBox1.Controls.Add(this.btn_readBackCard);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.btn_saveData);
            this.groupBox1.Controls.Add(this.btn_readCard);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.listbox_blood);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.listbox_nationality);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.listbox_major);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.listbox_gender);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.picture_personal);
            this.groupBox1.Controls.Add(this.txt_lastname);
            this.groupBox1.Controls.Add(this.txt_firstname);
            this.groupBox1.Controls.Add(this.listbox_title);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("TH Sarabun New", 17.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox1.Location = new System.Drawing.Point(6, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(906, 572);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ข้อมูลบุคคล";
            // 
            // lineId
            // 
            this.lineId.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lineId.Location = new System.Drawing.Point(774, 116);
            this.lineId.Name = "lineId";
            this.lineId.Size = new System.Drawing.Size(122, 32);
            this.lineId.TabIndex = 44;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label9.Location = new System.Drawing.Point(770, 90);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 28);
            this.label9.TabIndex = 43;
            this.label9.Text = "ไลน์";
            // 
            // preferWorkingProvince
            // 
            this.preferWorkingProvince.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.preferWorkingProvince.FormattingEnabled = true;
            this.preferWorkingProvince.Location = new System.Drawing.Point(19, 55);
            this.preferWorkingProvince.Name = "preferWorkingProvince";
            this.preferWorkingProvince.Size = new System.Drawing.Size(147, 34);
            this.preferWorkingProvince.TabIndex = 42;
            this.preferWorkingProvince.SelectedIndexChanged += new System.EventHandler(this.preferWorkingProvince_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(13, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 28);
            this.label1.TabIndex = 41;
            this.label1.Text = "จังหวัดที่ต้องการอาสา";
            // 
            // txt_email
            // 
            this.txt_email.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txt_email.Location = new System.Drawing.Point(627, 116);
            this.txt_email.Name = "txt_email";
            this.txt_email.Size = new System.Drawing.Size(141, 32);
            this.txt_email.TabIndex = 40;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label35.Location = new System.Drawing.Point(623, 90);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(44, 28);
            this.label35.TabIndex = 39;
            this.label35.Text = "อีเมล์";
            // 
            // txt_identity_card_number
            // 
            this.txt_identity_card_number.Culture = new System.Globalization.CultureInfo("en-US");
            this.txt_identity_card_number.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txt_identity_card_number.ForeColor = System.Drawing.Color.Black;
            this.txt_identity_card_number.Location = new System.Drawing.Point(325, 56);
            this.txt_identity_card_number.Mask = "0-0000-00000-00-0";
            this.txt_identity_card_number.Name = "txt_identity_card_number";
            this.txt_identity_card_number.Size = new System.Drawing.Size(142, 32);
            this.txt_identity_card_number.TabIndex = 38;
            // 
            // txt_birthday
            // 
            this.txt_birthday.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txt_birthday.Location = new System.Drawing.Point(21, 117);
            this.txt_birthday.Mask = "00/00/0000";
            this.txt_birthday.Name = "txt_birthday";
            this.txt_birthday.Size = new System.Drawing.Size(118, 32);
            this.txt_birthday.TabIndex = 35;
            this.txt_birthday.ValidatingType = typeof(System.DateTime);
            // 
            // txt_phone
            // 
            this.txt_phone.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txt_phone.Location = new System.Drawing.Point(491, 116);
            this.txt_phone.MaxLength = 10;
            this.txt_phone.Name = "txt_phone";
            this.txt_phone.Size = new System.Drawing.Size(118, 32);
            this.txt_phone.TabIndex = 25;
            this.txt_phone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IsNumeric);
            // 
            // btn_readBackCard
            // 
            this.btn_readBackCard.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btn_readBackCard.Image = ((System.Drawing.Image)(resources.GetObject("btn_readBackCard.Image")));
            this.btn_readBackCard.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_readBackCard.Location = new System.Drawing.Point(423, 510);
            this.btn_readBackCard.Name = "btn_readBackCard";
            this.btn_readBackCard.Size = new System.Drawing.Size(151, 41);
            this.btn_readBackCard.TabIndex = 6;
            this.btn_readBackCard.Text = "พิมพ์หลังบัตร";
            this.btn_readBackCard.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_readBackCard.UseVisualStyleBackColor = true;
            this.btn_readBackCard.Click += new System.EventHandler(this.btn_readBackCard_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txt_placename);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.txt_housenumber);
            this.groupBox2.Controls.Add(this.listbox_province);
            this.groupBox2.Controls.Add(this.txt_road);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.txt_soi);
            this.groupBox2.Controls.Add(this.txt_postcode);
            this.groupBox2.Controls.Add(this.txt_village);
            this.groupBox2.Controls.Add(this.txt_tambon);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.listbox_amphur);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.txt_moo);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(6, 334);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(890, 165);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ข้อมูลที่อยู่";
            // 
            // txt_placename
            // 
            this.txt_placename.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.txt_placename.Location = new System.Drawing.Point(176, 52);
            this.txt_placename.Name = "txt_placename";
            this.txt_placename.Size = new System.Drawing.Size(139, 30);
            this.txt_placename.TabIndex = 50;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label28.Location = new System.Drawing.Point(171, 25);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(76, 28);
            this.label28.TabIndex = 51;
            this.label28.Text = "ชื่อสถานที่";
            // 
            // txt_housenumber
            // 
            this.txt_housenumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.txt_housenumber.Location = new System.Drawing.Point(14, 54);
            this.txt_housenumber.Name = "txt_housenumber";
            this.txt_housenumber.Size = new System.Drawing.Size(45, 30);
            this.txt_housenumber.TabIndex = 49;
            // 
            // listbox_province
            // 
            this.listbox_province.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.listbox_province.FormattingEnabled = true;
            this.listbox_province.Location = new System.Drawing.Point(340, 114);
            this.listbox_province.Name = "listbox_province";
            this.listbox_province.Size = new System.Drawing.Size(134, 33);
            this.listbox_province.TabIndex = 48;
            this.listbox_province.SelectedIndexChanged += new System.EventHandler(this.listbox_province_SelectedIndexChanged);
            // 
            // txt_road
            // 
            this.txt_road.Font = new System.Drawing.Font("TH Sarabun New", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txt_road.Location = new System.Drawing.Point(702, 52);
            this.txt_road.Name = "txt_road";
            this.txt_road.Size = new System.Drawing.Size(126, 34);
            this.txt_road.TabIndex = 46;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label25.Location = new System.Drawing.Point(697, 25);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(41, 28);
            this.label25.TabIndex = 47;
            this.label25.Text = "ถนน";
            // 
            // txt_soi
            // 
            this.txt_soi.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txt_soi.Location = new System.Drawing.Point(493, 52);
            this.txt_soi.Name = "txt_soi";
            this.txt_soi.Size = new System.Drawing.Size(189, 33);
            this.txt_soi.TabIndex = 29;
            // 
            // txt_postcode
            // 
            this.txt_postcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.txt_postcode.Location = new System.Drawing.Point(493, 115);
            this.txt_postcode.Name = "txt_postcode";
            this.txt_postcode.Size = new System.Drawing.Size(121, 30);
            this.txt_postcode.TabIndex = 45;
            // 
            // txt_village
            // 
            this.txt_village.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.txt_village.Location = new System.Drawing.Point(340, 52);
            this.txt_village.Name = "txt_village";
            this.txt_village.Size = new System.Drawing.Size(134, 30);
            this.txt_village.TabIndex = 28;
            // 
            // txt_tambon
            // 
            this.txt_tambon.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txt_tambon.Location = new System.Drawing.Point(14, 115);
            this.txt_tambon.Name = "txt_tambon";
            this.txt_tambon.Size = new System.Drawing.Size(134, 32);
            this.txt_tambon.TabIndex = 43;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label17.Location = new System.Drawing.Point(488, 89);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(98, 28);
            this.label17.TabIndex = 41;
            this.label17.Text = "รหัสไปรษณีย์";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label18.Location = new System.Drawing.Point(336, 88);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(56, 28);
            this.label18.TabIndex = 40;
            this.label18.Text = "จังหวัด";
            // 
            // listbox_amphur
            // 
            this.listbox_amphur.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.listbox_amphur.FormattingEnabled = true;
            this.listbox_amphur.Location = new System.Drawing.Point(176, 114);
            this.listbox_amphur.Name = "listbox_amphur";
            this.listbox_amphur.Size = new System.Drawing.Size(139, 33);
            this.listbox_amphur.TabIndex = 39;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label19.Location = new System.Drawing.Point(171, 87);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(81, 28);
            this.label19.TabIndex = 38;
            this.label19.Text = "อำเภอ/เขต";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("TH Sarabun New", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label20.Location = new System.Drawing.Point(9, 89);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(87, 27);
            this.label20.TabIndex = 36;
            this.label20.Text = "ตำบล/แขวง";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label21.Location = new System.Drawing.Point(486, 25);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(41, 28);
            this.label21.TabIndex = 34;
            this.label21.Text = "ซอย";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label22.Location = new System.Drawing.Point(335, 25);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(58, 28);
            this.label22.TabIndex = 32;
            this.label22.Text = "หมู่บ้าน";
            // 
            // txt_moo
            // 
            this.txt_moo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.txt_moo.Location = new System.Drawing.Point(75, 54);
            this.txt_moo.Name = "txt_moo";
            this.txt_moo.Size = new System.Drawing.Size(56, 30);
            this.txt_moo.TabIndex = 28;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label23.Location = new System.Drawing.Point(70, 27);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(41, 28);
            this.label23.TabIndex = 31;
            this.label23.Text = "หมู่ที่";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("TH Sarabun New", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(9, 25);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(46, 27);
            this.label24.TabIndex = 29;
            this.label24.Text = "เลขที่";
            // 
            // btn_saveData
            // 
            this.btn_saveData.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btn_saveData.Image = ((System.Drawing.Image)(resources.GetObject("btn_saveData.Image")));
            this.btn_saveData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_saveData.Location = new System.Drawing.Point(747, 510);
            this.btn_saveData.Name = "btn_saveData";
            this.btn_saveData.Size = new System.Drawing.Size(145, 41);
            this.btn_saveData.TabIndex = 5;
            this.btn_saveData.Text = "บันทึกข้อมูล";
            this.btn_saveData.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_saveData.UseVisualStyleBackColor = true;
            this.btn_saveData.Click += new System.EventHandler(this.btn_saveData_Click);
            // 
            // btn_readCard
            // 
            this.btn_readCard.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btn_readCard.Image = ((System.Drawing.Image)(resources.GetObject("btn_readCard.Image")));
            this.btn_readCard.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_readCard.Location = new System.Drawing.Point(580, 510);
            this.btn_readCard.Name = "btn_readCard";
            this.btn_readCard.Size = new System.Drawing.Size(161, 41);
            this.btn_readCard.TabIndex = 4;
            this.btn_readCard.Text = "อ่านข้อมูลบัตร";
            this.btn_readCard.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_readCard.UseVisualStyleBackColor = true;
            this.btn_readCard.Click += new System.EventHandler(this.btn_readCard_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label14.Location = new System.Drawing.Point(483, 91);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(105, 28);
            this.label14.TabIndex = 21;
            this.label14.Text = "โทรศัพท์มือถือ";
            // 
            // listbox_blood
            // 
            this.listbox_blood.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.listbox_blood.FormattingEnabled = true;
            this.listbox_blood.Location = new System.Drawing.Point(326, 116);
            this.listbox_blood.Name = "listbox_blood";
            this.listbox_blood.Size = new System.Drawing.Size(112, 34);
            this.listbox_blood.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label10.Location = new System.Drawing.Point(321, 90);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 28);
            this.label10.TabIndex = 16;
            this.label10.Text = "หมู่เลือด";
            // 
            // listbox_nationality
            // 
            this.listbox_nationality.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.listbox_nationality.FormattingEnabled = true;
            this.listbox_nationality.Location = new System.Drawing.Point(184, 115);
            this.listbox_nationality.Name = "listbox_nationality";
            this.listbox_nationality.Size = new System.Drawing.Size(121, 34);
            this.listbox_nationality.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label8.Location = new System.Drawing.Point(177, 88);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 28);
            this.label8.TabIndex = 12;
            this.label8.Text = "สัญชาติ";
            // 
            // listbox_major
            // 
            this.listbox_major.Font = new System.Drawing.Font("TH Sarabun New", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listbox_major.FormattingEnabled = true;
            this.listbox_major.Location = new System.Drawing.Point(18, 293);
            this.listbox_major.Name = "listbox_major";
            this.listbox_major.Size = new System.Drawing.Size(171, 35);
            this.listbox_major.TabIndex = 20;
            this.listbox_major.SelectedIndexChanged += new System.EventHandler(this.listbox_major_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("TH Sarabun New", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label7.Location = new System.Drawing.Point(16, 88);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 27);
            this.label7.TabIndex = 11;
            this.label7.Text = "วันเกิด";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label11.Location = new System.Drawing.Point(16, 262);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(118, 28);
            this.label11.TabIndex = 19;
            this.label11.Text = "อาชีพ/ความถนัด";
            // 
            // listbox_gender
            // 
            this.listbox_gender.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.listbox_gender.FormattingEnabled = true;
            this.listbox_gender.Location = new System.Drawing.Point(773, 52);
            this.listbox_gender.Name = "listbox_gender";
            this.listbox_gender.Size = new System.Drawing.Size(112, 34);
            this.listbox_gender.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("TH Sarabun New", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(766, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 27);
            this.label6.TabIndex = 9;
            this.label6.Text = "เพศ";
            // 
            // picture_personal
            // 
            this.picture_personal.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picture_personal.Location = new System.Drawing.Point(762, 170);
            this.picture_personal.Name = "picture_personal";
            this.picture_personal.Size = new System.Drawing.Size(130, 158);
            this.picture_personal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picture_personal.TabIndex = 8;
            this.picture_personal.TabStop = false;
            // 
            // txt_lastname
            // 
            this.txt_lastname.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txt_lastname.Location = new System.Drawing.Point(628, 54);
            this.txt_lastname.Name = "txt_lastname";
            this.txt_lastname.Size = new System.Drawing.Size(124, 32);
            this.txt_lastname.TabIndex = 7;
            // 
            // txt_firstname
            // 
            this.txt_firstname.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txt_firstname.Location = new System.Drawing.Point(491, 54);
            this.txt_firstname.Name = "txt_firstname";
            this.txt_firstname.Size = new System.Drawing.Size(121, 32);
            this.txt_firstname.TabIndex = 6;
            // 
            // listbox_title
            // 
            this.listbox_title.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.listbox_title.FormattingEnabled = true;
            this.listbox_title.Location = new System.Drawing.Point(183, 53);
            this.listbox_title.Name = "listbox_title";
            this.listbox_title.Size = new System.Drawing.Size(121, 34);
            this.listbox_title.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(623, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 28);
            this.label5.TabIndex = 4;
            this.label5.Text = "นามสกุล";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(488, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 28);
            this.label4.TabIndex = 3;
            this.label4.Text = "ชื่อ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(178, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 28);
            this.label3.TabIndex = 2;
            this.label3.Text = "คำนำหน้าชื่อ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(318, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 28);
            this.label2.TabIndex = 0;
            this.label2.Text = "เลขบัตรประชาชน";
            // 
            // StatusProgressBar
            // 
            this.StatusProgressBar.Location = new System.Drawing.Point(7, 694);
            this.StatusProgressBar.Name = "StatusProgressBar";
            this.StatusProgressBar.Size = new System.Drawing.Size(903, 10);
            this.StatusProgressBar.TabIndex = 28;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.DarkRed;
            this.pictureBox3.BackgroundImage = global::SmartCardVtris.Properties.Resources.logo_vtris;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox3.Image = global::SmartCardVtris.Properties.Resources.logo_vtris;
            this.pictureBox3.Location = new System.Drawing.Point(12, 11);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(152, 45);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 8;
            this.pictureBox3.TabStop = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.DarkRed;
            this.label15.Font = new System.Drawing.Font("TH Sarabun New", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(180, 13);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(520, 43);
            this.label15.TabIndex = 9;
            this.label15.Text = "โปรแกรมลงทะเบียนอาสาสมัครเคลือนที่ สภากาชาดไทย";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tabControl1.Location = new System.Drawing.Point(0, 81);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(926, 607);
            this.tabControl1.TabIndex = 10;
            this.tabControl1.Click += new System.EventHandler(this.tabControl1_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Font = new System.Drawing.Font("TH Sarabun New", 20F);
            this.tabPage1.Location = new System.Drawing.Point(4, 41);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(918, 562);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "สร้างบุคคล";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Font = new System.Drawing.Font("TH Sarabun New", 20F);
            this.tabPage2.Location = new System.Drawing.Point(4, 38);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(918, 522);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "ค้นหาบุคคล";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txt_idcard);
            this.groupBox5.Controls.Add(this.btn_PrintCardSerach);
            this.groupBox5.Controls.Add(this.txt_idno);
            this.groupBox5.Controls.Add(this.label30);
            this.groupBox5.Controls.Add(this.label31);
            this.groupBox5.Controls.Add(this.label32);
            this.groupBox5.Controls.Add(this.label33);
            this.groupBox5.Controls.Add(this.progressBar1);
            this.groupBox5.Controls.Add(this.txt_phones);
            this.groupBox5.Controls.Add(this.button1);
            this.groupBox5.Controls.Add(this.button2);
            this.groupBox5.Controls.Add(this.button3);
            this.groupBox5.Controls.Add(this.label46);
            this.groupBox5.Controls.Add(this.picture_personals);
            this.groupBox5.Controls.Add(this.txt_lastnames);
            this.groupBox5.Controls.Add(this.txt_firstnames);
            this.groupBox5.Controls.Add(this.listbox_titles);
            this.groupBox5.Controls.Add(this.label53);
            this.groupBox5.Controls.Add(this.label54);
            this.groupBox5.Controls.Add(this.label55);
            this.groupBox5.Controls.Add(this.label56);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold);
            this.groupBox5.Location = new System.Drawing.Point(5, 130);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(906, 317);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "แสดงข้อมูลบุคคล";
            // 
            // txt_idcard
            // 
            this.txt_idcard.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txt_idcard.Location = new System.Drawing.Point(382, 82);
            this.txt_idcard.Mask = "0-0000-00000-00-0";
            this.txt_idcard.Name = "txt_idcard";
            this.txt_idcard.Size = new System.Drawing.Size(142, 30);
            this.txt_idcard.TabIndex = 39;
            // 
            // btn_PrintCardSerach
            // 
            this.btn_PrintCardSerach.Image = ((System.Drawing.Image)(resources.GetObject("btn_PrintCardSerach.Image")));
            this.btn_PrintCardSerach.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_PrintCardSerach.Location = new System.Drawing.Point(384, 245);
            this.btn_PrintCardSerach.Name = "btn_PrintCardSerach";
            this.btn_PrintCardSerach.Size = new System.Drawing.Size(113, 43);
            this.btn_PrintCardSerach.TabIndex = 34;
            this.btn_PrintCardSerach.Text = "พิมพ์บัตร";
            this.btn_PrintCardSerach.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_PrintCardSerach.UseVisualStyleBackColor = true;
            this.btn_PrintCardSerach.Click += new System.EventHandler(this.btn_PrintCardSerach_Click);
            // 
            // txt_idno
            // 
            this.txt_idno.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.txt_idno.Location = new System.Drawing.Point(219, 82);
            this.txt_idno.MaxLength = 13;
            this.txt_idno.Name = "txt_idno";
            this.txt_idno.Size = new System.Drawing.Size(142, 30);
            this.txt_idno.TabIndex = 33;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            this.label30.Location = new System.Drawing.Point(212, 56);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(95, 25);
            this.label30.TabIndex = 32;
            this.label30.Text = "เลขที่สมัคร";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold);
            this.label31.Location = new System.Drawing.Point(135, 216);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(31, 31);
            this.label31.TabIndex = 31;
            this.label31.Text = "ปี";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold);
            this.label32.Location = new System.Drawing.Point(89, 216);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(24, 31);
            this.label32.TabIndex = 30;
            this.label32.Text = "-";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold);
            this.label33.Location = new System.Drawing.Point(39, 216);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(56, 31);
            this.label33.TabIndex = 29;
            this.label33.Text = "อายุ";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(1, 437);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(903, 10);
            this.progressBar1.TabIndex = 28;
            // 
            // txt_phones
            // 
            this.txt_phones.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.txt_phones.Location = new System.Drawing.Point(554, 142);
            this.txt_phones.Name = "txt_phones";
            this.txt_phones.Size = new System.Drawing.Size(118, 30);
            this.txt_phones.TabIndex = 25;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(469, 387);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(130, 41);
            this.button1.TabIndex = 6;
            this.button1.Text = "พิมพ์หลังบัตร";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(780, 385);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(117, 41);
            this.button2.TabIndex = 5;
            this.button2.Text = "บันทึกข้อมูล";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(618, 387);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(137, 41);
            this.button3.TabIndex = 4;
            this.button3.Text = "อ่านข้อมูลบัตร";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            this.label46.Location = new System.Drawing.Point(548, 114);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(117, 25);
            this.label46.TabIndex = 21;
            this.label46.Text = "เบอร์โทรศัพท์";
            // 
            // picture_personals
            // 
            this.picture_personals.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picture_personals.Location = new System.Drawing.Point(39, 56);
            this.picture_personals.Name = "picture_personals";
            this.picture_personals.Size = new System.Drawing.Size(115, 134);
            this.picture_personals.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picture_personals.TabIndex = 8;
            this.picture_personals.TabStop = false;
            // 
            // txt_lastnames
            // 
            this.txt_lastnames.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.txt_lastnames.Location = new System.Drawing.Point(384, 142);
            this.txt_lastnames.Name = "txt_lastnames";
            this.txt_lastnames.Size = new System.Drawing.Size(112, 30);
            this.txt_lastnames.TabIndex = 7;
            // 
            // txt_firstnames
            // 
            this.txt_firstnames.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.txt_firstnames.Location = new System.Drawing.Point(219, 142);
            this.txt_firstnames.Name = "txt_firstnames";
            this.txt_firstnames.Size = new System.Drawing.Size(121, 30);
            this.txt_firstnames.TabIndex = 6;
            // 
            // listbox_titles
            // 
            this.listbox_titles.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.listbox_titles.FormattingEnabled = true;
            this.listbox_titles.Location = new System.Drawing.Point(554, 80);
            this.listbox_titles.Name = "listbox_titles";
            this.listbox_titles.Size = new System.Drawing.Size(121, 33);
            this.listbox_titles.TabIndex = 5;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            this.label53.Location = new System.Drawing.Point(379, 116);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(78, 25);
            this.label53.TabIndex = 4;
            this.label53.Text = "นามสกุล";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            this.label54.Location = new System.Drawing.Point(214, 114);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(34, 25);
            this.label54.TabIndex = 3;
            this.label54.Text = "ชื่อ";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            this.label55.Location = new System.Drawing.Point(549, 54);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(114, 25);
            this.label55.TabIndex = 2;
            this.label55.Text = "คำนำหน้าชื่อ";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            this.label56.Location = new System.Drawing.Point(377, 54);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(232, 25);
            this.label56.TabIndex = 0;
            this.label56.Text = "เลขบัตรประชาชน/พาสปอร์ต";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btn_searchData);
            this.groupBox4.Controls.Add(this.txt_search);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.groupBox4.Location = new System.Drawing.Point(9, 15);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(902, 91);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "ค้นหาบุคคล";
            // 
            // btn_searchData
            // 
            this.btn_searchData.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.btn_searchData.Image = ((System.Drawing.Image)(resources.GetObject("btn_searchData.Image")));
            this.btn_searchData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_searchData.Location = new System.Drawing.Point(614, 34);
            this.btn_searchData.Name = "btn_searchData";
            this.btn_searchData.Size = new System.Drawing.Size(78, 35);
            this.btn_searchData.TabIndex = 2;
            this.btn_searchData.Text = "ค้นหา";
            this.btn_searchData.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_searchData.UseVisualStyleBackColor = true;
            this.btn_searchData.Click += new System.EventHandler(this.btn_searchData_Click);
            // 
            // txt_search
            // 
            this.txt_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.txt_search.Location = new System.Drawing.Point(226, 39);
            this.txt_search.Name = "txt_search";
            this.txt_search.Size = new System.Drawing.Size(369, 29);
            this.txt_search.TabIndex = 1;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label16.Location = new System.Drawing.Point(50, 38);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(261, 29);
            this.label16.TabIndex = 0;
            this.label16.Text = "ค้นหาเลขบัตรประชาชน :";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btn_exportExcel);
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Location = new System.Drawing.Point(4, 38);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(918, 522);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "รายงานบุคคล";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btn_exportExcel
            // 
            this.btn_exportExcel.Image = ((System.Drawing.Image)(resources.GetObject("btn_exportExcel.Image")));
            this.btn_exportExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_exportExcel.Location = new System.Drawing.Point(339, 413);
            this.btn_exportExcel.Name = "btn_exportExcel";
            this.btn_exportExcel.Size = new System.Drawing.Size(182, 43);
            this.btn_exportExcel.TabIndex = 1;
            this.btn_exportExcel.Text = "ออกรายงาน Excel";
            this.btn_exportExcel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_exportExcel.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.data_volunteer);
            this.groupBox3.Location = new System.Drawing.Point(9, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(902, 395);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            // 
            // data_volunteer
            // 
            this.data_volunteer.AllowUserToAddRows = false;
            this.data_volunteer.AllowUserToDeleteRows = false;
            this.data_volunteer.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.data_volunteer.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.data_volunteer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.data_volunteer.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.data_volunteer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.data_volunteer.Location = new System.Drawing.Point(7, 35);
            this.data_volunteer.Name = "data_volunteer";
            this.data_volunteer.ReadOnly = true;
            this.data_volunteer.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.data_volunteer.Size = new System.Drawing.Size(889, 354);
            this.data_volunteer.TabIndex = 0;
            // 
            // txtBoxLog
            // 
            this.txtBoxLog.AutoSize = true;
            this.txtBoxLog.Location = new System.Drawing.Point(437, 349);
            this.txtBoxLog.Name = "txtBoxLog";
            this.txtBoxLog.Size = new System.Drawing.Size(0, 32);
            this.txtBoxLog.TabIndex = 38;
            // 
            // printbackcard
            // 
            this.printbackcard.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printbackcard_PrintPage);
            // 
            // printfrontcard
            // 
            this.printfrontcard.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printfrontcard_PrintPage);
            // 
            // listbox_club
            // 
            this.listbox_club.Font = new System.Drawing.Font("TH Sarabun New", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listbox_club.FormattingEnabled = true;
            this.listbox_club.Location = new System.Drawing.Point(18, 32);
            this.listbox_club.Name = "listbox_club";
            this.listbox_club.Size = new System.Drawing.Size(212, 35);
            this.listbox_club.TabIndex = 33;
            // 
            // groupClub
            // 
            this.groupClub.Controls.Add(this.listbox_club);
            this.groupClub.Font = new System.Drawing.Font("TH Sarabun New", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupClub.Location = new System.Drawing.Point(246, 211);
            this.groupClub.Name = "groupClub";
            this.groupClub.Size = new System.Drawing.Size(241, 88);
            this.groupClub.TabIndex = 51;
            this.groupClub.TabStop = false;
            this.groupClub.Text = "ชมรม";
            // 
            // list_occupationwork
            // 
            this.list_occupationwork.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.list_occupationwork.FormattingEnabled = true;
            this.list_occupationwork.Location = new System.Drawing.Point(20, 224);
            this.list_occupationwork.Name = "list_occupationwork";
            this.list_occupationwork.Size = new System.Drawing.Size(172, 36);
            this.list_occupationwork.TabIndex = 47;
            // 
            // list_occupationNotwork
            // 
            this.list_occupationNotwork.Font = new System.Drawing.Font("TH Sarabun New", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.list_occupationNotwork.FormattingEnabled = true;
            this.list_occupationNotwork.Location = new System.Drawing.Point(20, 224);
            this.list_occupationNotwork.Name = "list_occupationNotwork";
            this.list_occupationNotwork.Size = new System.Drawing.Size(172, 35);
            this.list_occupationNotwork.TabIndex = 48;
            this.list_occupationNotwork.SelectedIndexChanged += new System.EventHandler(this.list_occupationNotwork_SelectedIndexChanged);
            // 
            // majorclub
            // 
            this.majorclub.AutoSize = true;
            this.majorclub.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.majorclub.Location = new System.Drawing.Point(250, 164);
            this.majorclub.Name = "majorclub";
            this.majorclub.Size = new System.Drawing.Size(101, 32);
            this.majorclub.TabIndex = 49;
            this.majorclub.TabStop = true;
            this.majorclub.Text = "สังกัดชมรม";
            this.majorclub.UseVisualStyleBackColor = true;
            this.majorclub.CheckedChanged += new System.EventHandler(this.majorclub_CheckedChanged);
            // 
            // majorNotclub
            // 
            this.majorNotclub.AutoSize = true;
            this.majorNotclub.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.majorNotclub.Location = new System.Drawing.Point(353, 164);
            this.majorNotclub.Name = "majorNotclub";
            this.majorNotclub.Size = new System.Drawing.Size(133, 32);
            this.majorNotclub.TabIndex = 50;
            this.majorNotclub.TabStop = true;
            this.majorNotclub.Text = "ไม่ได้สังกัดชมรม";
            this.majorNotclub.UseVisualStyleBackColor = true;
            this.majorNotclub.CheckedChanged += new System.EventHandler(this.majorNotclub_CheckedChanged);
            // 
            // occupationGroupwork
            // 
            this.occupationGroupwork.AutoSize = true;
            this.occupationGroupwork.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.occupationGroupwork.Location = new System.Drawing.Point(5, 19);
            this.occupationGroupwork.Name = "occupationGroupwork";
            this.occupationGroupwork.Size = new System.Drawing.Size(89, 32);
            this.occupationGroupwork.TabIndex = 45;
            this.occupationGroupwork.TabStop = true;
            this.occupationGroupwork.Text = "ทำงานอยู่";
            this.occupationGroupwork.UseVisualStyleBackColor = true;
            this.occupationGroupwork.CheckedChanged += new System.EventHandler(this.occupationGroupwork_CheckedChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.occupationGroupwork);
            this.groupBox6.Controls.Add(this.occupationGroupnotwork);
            this.groupBox6.Location = new System.Drawing.Point(19, 153);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(200, 54);
            this.groupBox6.TabIndex = 52;
            this.groupBox6.TabStop = false;
            // 
            // occupationGroupnotwork
            // 
            this.occupationGroupnotwork.AutoSize = true;
            this.occupationGroupnotwork.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.occupationGroupnotwork.Location = new System.Drawing.Point(97, 19);
            this.occupationGroupnotwork.Name = "occupationGroupnotwork";
            this.occupationGroupnotwork.Size = new System.Drawing.Size(103, 32);
            this.occupationGroupnotwork.TabIndex = 46;
            this.occupationGroupnotwork.TabStop = true;
            this.occupationGroupnotwork.Text = "ไม่ได้ทำงาน";
            this.occupationGroupnotwork.UseVisualStyleBackColor = true;
            this.occupationGroupnotwork.CheckedChanged += new System.EventHandler(this.occupationGroupnotwork_CheckedChanged);
            // 
            // groupeducation
            // 
            this.groupeducation.Controls.Add(this.education_masterdegree);
            this.groupeducation.Controls.Add(this.education_bachelor);
            this.groupeducation.Controls.Add(this.education_underdegree);
            this.groupeducation.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupeducation.Font = new System.Drawing.Font("TH Sarabun New", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupeducation.Location = new System.Drawing.Point(499, 172);
            this.groupeducation.Name = "groupeducation";
            this.groupeducation.Size = new System.Drawing.Size(253, 145);
            this.groupeducation.TabIndex = 53;
            this.groupeducation.TabStop = false;
            this.groupeducation.Text = "ระดับการศึกษา";
            // 
            // education_underdegree
            // 
            this.education_underdegree.AutoSize = true;
            this.education_underdegree.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.education_underdegree.Font = new System.Drawing.Font("TH Sarabun New", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.education_underdegree.Location = new System.Drawing.Point(10, 33);
            this.education_underdegree.Name = "education_underdegree";
            this.education_underdegree.Size = new System.Drawing.Size(239, 31);
            this.education_underdegree.TabIndex = 0;
            this.education_underdegree.TabStop = true;
            this.education_underdegree.Text = "ประกาศนียบัตร/ต่ำกว่าปริญญาตรี";
            this.education_underdegree.UseVisualStyleBackColor = true;
            // 
            // education_bachelor
            // 
            this.education_bachelor.AutoSize = true;
            this.education_bachelor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.education_bachelor.Font = new System.Drawing.Font("TH Sarabun New", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.education_bachelor.Location = new System.Drawing.Point(10, 66);
            this.education_bachelor.Name = "education_bachelor";
            this.education_bachelor.Size = new System.Drawing.Size(180, 31);
            this.education_bachelor.TabIndex = 1;
            this.education_bachelor.TabStop = true;
            this.education_bachelor.Text = "ปริญญาตรีหรือเทียบเท่า";
            this.education_bachelor.UseVisualStyleBackColor = true;
            // 
            // education_masterdegree
            // 
            this.education_masterdegree.AutoSize = true;
            this.education_masterdegree.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.education_masterdegree.Font = new System.Drawing.Font("TH Sarabun New", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.education_masterdegree.Location = new System.Drawing.Point(10, 102);
            this.education_masterdegree.Name = "education_masterdegree";
            this.education_masterdegree.Size = new System.Drawing.Size(131, 31);
            this.education_masterdegree.TabIndex = 2;
            this.education_masterdegree.TabStop = true;
            this.education_masterdegree.Text = "ปริญญาโทขึ้นไป";
            this.education_masterdegree.UseVisualStyleBackColor = true;
            // 
            // Form_create
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(927, 709);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.StatusProgressBar);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 5, 2, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_create";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "โปรแกรมลงทะเบียนอาสาสมัครเคลื่อนที่ สภากาชาดไทย";
            this.Load += new System.EventHandler(this.Form_create_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picture_personal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picture_personals)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.data_volunteer)).EndInit();
            this.groupClub.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupeducation.ResumeLayout(false);
            this.groupeducation.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_readCard;
        private System.Windows.Forms.Button btn_saveData;
        private System.Windows.Forms.Button btn_readBackCard;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox listbox_title;
        private System.Windows.Forms.TextBox txt_lastname;
        private System.Windows.Forms.TextBox txt_firstname;
        private System.Windows.Forms.PictureBox picture_personal;
        private System.Windows.Forms.ComboBox listbox_gender;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox listbox_nationality;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox listbox_blood;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox listbox_major;
        private System.Windows.Forms.TextBox txt_phone;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox txt_postcode;
        private System.Windows.Forms.TextBox txt_tambon;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox listbox_amphur;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txt_moo;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txt_road;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txt_soi;
        private System.Windows.Forms.TextBox txt_village;
        private System.Windows.Forms.ComboBox listbox_province;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btn_searchData;
        private System.Windows.Forms.TextBox txt_search;
        private System.Windows.Forms.TextBox txt_housenumber;
        private System.Windows.Forms.ProgressBar StatusProgressBar;
        private Label txtBoxLog;
        private TextBox txt_placename;
        private Label label28;
        private System.Drawing.Printing.PrintDocument printbackcard;
        private System.Drawing.Printing.PrintDocument printfrontcard;
        private GroupBox groupBox5;
        private Label label31;
        private Label label32;
        private Label label33;
        private ProgressBar progressBar1;
        private TextBox txt_phones;
        private Button button1;
        private Button button2;
        private Button button3;
        private Label label46;
        private PictureBox picture_personals;
        private TextBox txt_lastnames;
        private TextBox txt_firstnames;
        private ComboBox listbox_titles;
        private Label label53;
        private Label label54;
        private Label label55;
        private Label label56;
        private TextBox txt_idno;
        private Label label30;
        private Button btn_PrintCardSerach;
        private Button btn_exportExcel;
        private DataGridView data_volunteer;
        private MaskedTextBox txt_birthday;
        private MaskedTextBox txt_identity_card_number;
        private MaskedTextBox txt_idcard;
        private Label label35;
        private TextBox txt_email;
        private ComboBox preferWorkingProvince;
        private Label label1;
        private TextBox lineId;
        private Label label9;
        private GroupBox groupClub;
        private ComboBox listbox_club;
        private GroupBox groupBox6;
        private RadioButton occupationGroupwork;
        private RadioButton occupationGroupnotwork;
        private RadioButton majorNotclub;
        private RadioButton majorclub;
        private ComboBox list_occupationNotwork;
        private ComboBox list_occupationwork;
        private GroupBox groupeducation;
        private RadioButton education_masterdegree;
        private RadioButton education_bachelor;
        private RadioButton education_underdegree;
    }
}

